﻿
namespace Stox.Core
{
    public static class StoxVersion
    {
        /// <summary>
        /// Gets or sets the store version
        /// </summary>
        public static string CurrentVersion 
        {
            get
            {
                return "1.0.0.0.0";
            }
        }
    }
}
