﻿using Stox.Core.Configuration;

namespace Stox.Core.IO
{
    public class FileSystemSettings : ISettings
    {
        public string DirectoryName { get; set; }
    }
}