//Contributor:  Nicholas Mayne


namespace Stox.Services.Authentication.External
{
    public partial interface IClaimsTranslator<T>
    {
        UserClaims Translate(T response);
    }
}