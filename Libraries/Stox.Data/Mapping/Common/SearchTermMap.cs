﻿using Stox.Core.Domain.Common;
using System.Data.Entity.ModelConfiguration;

namespace Stox.Data.Mapping.Common
{
    public partial class SearchTermMap : EntityTypeConfiguration<SearchTerm>
    {
        public SearchTermMap()
        {
            this.ToTable("SearchTerm");
            this.HasKey(st => st.Id);
        }
    }
}
