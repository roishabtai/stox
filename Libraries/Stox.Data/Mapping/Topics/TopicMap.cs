﻿using Stox.Core.Domain.Topics;
using System.Data.Entity.ModelConfiguration;

namespace Stox.Data.Mapping.Topics
{
    public class TopicMap : EntityTypeConfiguration<Topic>
    {
        public TopicMap()
        {
            this.ToTable("Topic");
            this.HasKey(t => t.Id);
        }
    }
}
