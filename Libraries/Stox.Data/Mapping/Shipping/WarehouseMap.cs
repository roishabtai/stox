﻿using Stox.Core.Domain.Shipping;
using System.Data.Entity.ModelConfiguration;

namespace Stox.Data.Mapping.Shipping
{
    public class WarehouseMap : EntityTypeConfiguration<Warehouse>
    {
        public WarehouseMap()
        {
            this.ToTable("Warehouse");
            this.HasKey(wh => wh.Id);
            this.Property(wh => wh.Name).IsRequired().HasMaxLength(400);
        }
    }
}
