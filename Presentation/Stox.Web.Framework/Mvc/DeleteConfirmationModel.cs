﻿using Stox.Web.Framework.Mvc;

namespace Stox.Web.Framework.Mvc
{
    public class DeleteConfirmationModel : BaseStoxEntityModel
    {
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
    }
}