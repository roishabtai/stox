﻿namespace Stox.Web.Framework.Mvc
{
    public interface IModelAttribute
    {
        string Name { get; }
    }
}
