﻿//Contributor: http://aspnetadvimage.codeplex.com/

namespace Stox.Web.Framework.UI.Editor
{
    public class NetAdvImage
    {
        public string FileName { get; set; }
        public string Path { get; set; }
    }
}