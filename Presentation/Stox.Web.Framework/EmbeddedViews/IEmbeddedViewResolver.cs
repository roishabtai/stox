
namespace Stox.Web.Framework.EmbeddedViews
{
    public interface IEmbeddedViewResolver
    {
        EmbeddedViewTable GetEmbeddedViews();
    }
}