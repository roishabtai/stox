﻿namespace Stox.Web.Framework.Localization
{
    public interface ILocalizedModelLocal
    {
        int LanguageId { get; set; }
    }
}
