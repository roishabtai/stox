﻿using System.Collections.Generic;
using System.Web.Mvc;
using Stox.Services.Payments;

namespace Stox.Web.Framework.Controllers
{
    public abstract class BaseStoxPaymentController : Controller
    {
        public abstract IList<string> ValidatePaymentForm(FormCollection form);
        public abstract ProcessPaymentRequest GetPaymentInfo(FormCollection form);
    }
}
