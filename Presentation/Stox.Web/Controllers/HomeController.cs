﻿using System.Web.Mvc;
using Stox.Web.Framework.Security;

namespace Stox.Web.Controllers
{
    public partial class HomeController : BaseStoxController
    {
        [StoxHttpsRequirement(SslRequirement.No)]
        public ActionResult Index()
        {
            return View();
        }
    }
}
