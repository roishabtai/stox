﻿using System.Web.Mvc;
using FluentValidation.Attributes;
using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;
using Stox.Web.Validators.Common;

namespace Stox.Web.Models.Common
{
    [Validator(typeof(ContactUsValidator))]
    public partial class ContactUsModel : BaseStoxModel
    {
        [AllowHtml]
        [StoxResourceDisplayName("ContactUs.Email")]
        public string Email { get; set; }

        [AllowHtml]
        [StoxResourceDisplayName("ContactUs.Enquiry")]
        public string Enquiry { get; set; }

        [AllowHtml]
        [StoxResourceDisplayName("ContactUs.FullName")]
        public string FullName { get; set; }

        public bool SuccessfullySent { get; set; }
        public string Result { get; set; }

        public bool DisplayCaptcha { get; set; }
    }
}