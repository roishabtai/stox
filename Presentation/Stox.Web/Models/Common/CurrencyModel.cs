﻿using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Common
{
    public partial class CurrencyModel : BaseStoxEntityModel
    {
        public string Name { get; set; }

        public string CurrencySymbol { get; set; }
    }
}