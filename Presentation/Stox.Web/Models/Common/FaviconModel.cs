﻿using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Common
{
    public partial class FaviconModel : BaseStoxModel
    {
        public string FaviconUrl { get; set; }
    }
}