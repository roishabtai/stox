﻿using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Common
{
    public partial class LanguageModel : BaseStoxEntityModel
    {
        public string Name { get; set; }

        public string FlagImageFileName { get; set; }

    }
}