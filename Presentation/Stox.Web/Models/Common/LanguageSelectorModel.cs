﻿using System.Collections.Generic;
using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Common
{
    public partial class LanguageSelectorModel : BaseStoxModel
    {
        public LanguageSelectorModel()
        {
            AvailableLanguages = new List<LanguageModel>();
        }

        public IList<LanguageModel> AvailableLanguages { get; set; }

        public int CurrentLanguageId { get; set; }

        public bool UseImages { get; set; }
    }
}