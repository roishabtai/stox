﻿using Stox.Core.Domain.Tax;
using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Common
{
    public partial class TaxTypeSelectorModel : BaseStoxModel
    {
        public bool Enabled { get; set; }

        public TaxDisplayType CurrentTaxType { get; set; }
    }
}