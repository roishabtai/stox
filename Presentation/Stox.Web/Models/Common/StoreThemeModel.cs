﻿using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Common
{
    public partial class StoreThemeModel : BaseStoxModel
    {
        public string Name { get; set; }
        public string Title { get; set; }
    }
}