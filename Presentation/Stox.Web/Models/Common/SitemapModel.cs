﻿using System.Collections.Generic;
using Stox.Web.Framework.Mvc;
using Stox.Web.Models.Catalog;
using Stox.Web.Models.Topics;

namespace Stox.Web.Models.Common
{
    public partial class SitemapModel : BaseStoxModel
    {
        public SitemapModel()
        {
            Products = new List<ProductOverviewModel>();
            Categories = new List<CategoryModel>();
            Manufacturers = new List<ManufacturerModel>();
            Topics = new List<TopicModel>();
        }
        public IList<ProductOverviewModel> Products { get; set; }
        public IList<CategoryModel> Categories { get; set; }
        public IList<ManufacturerModel> Manufacturers { get; set; }
        public IList<TopicModel> Topics { get; set; }
    }
}