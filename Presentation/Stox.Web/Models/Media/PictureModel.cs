﻿using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Media
{
    public partial class PictureModel : BaseStoxModel
    {
        public string ImageUrl { get; set; }

        public string FullSizeImageUrl { get; set; }

        public string Title { get; set; }

        public string AlternateText { get; set; }
    }
}