﻿using System.Collections.Generic;
using Stox.Web.Models.Common;

namespace Stox.Web.Models.Profile
{
    public partial class ProfilePostsModel
    {
        public IList<PostsModel> Posts { get; set; }
        public PagerModel PagerModel { get; set; }
    }
}