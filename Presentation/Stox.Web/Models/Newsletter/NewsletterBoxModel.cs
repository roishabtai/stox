﻿using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Newsletter
{
    public partial class NewsletterBoxModel : BaseStoxModel
    {
        public string NewsletterEmail { get; set; }
    }
}