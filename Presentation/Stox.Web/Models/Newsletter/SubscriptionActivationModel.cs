﻿using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Newsletter
{
    public partial class SubscriptionActivationModel : BaseStoxModel
    {
        public string Result { get; set; }
    }
}