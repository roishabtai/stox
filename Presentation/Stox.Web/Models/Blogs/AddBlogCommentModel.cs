﻿using System.Web.Mvc;
using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Blogs
{
    public partial class AddBlogCommentModel : BaseStoxEntityModel
    {
        [StoxResourceDisplayName("Blog.Comments.CommentText")]
        [AllowHtml]
        public string CommentText { get; set; }

        public bool DisplayCaptcha { get; set; }
    }
}