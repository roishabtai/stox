﻿using System.Collections.Generic;
using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Blogs
{
    public partial class BlogPostListModel : BaseStoxModel
    {
        public BlogPostListModel()
        {
            PagingFilteringContext = new BlogPagingFilteringModel();
            BlogPosts = new List<BlogPostModel>();
        }

        public int WorkingLanguageId { get; set; }
        public BlogPagingFilteringModel PagingFilteringContext { get; set; }
        public IList<BlogPostModel> BlogPosts { get; set; }
    }
}