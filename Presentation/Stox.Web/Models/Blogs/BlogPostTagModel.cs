﻿using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Blogs
{
    public partial class BlogPostTagModel : BaseStoxModel
    {
        public string Name { get; set; }

        public int BlogPostCount { get; set; }
    }
}