﻿using System.Collections.Generic;
using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Blogs
{
    public partial class BlogPostYearModel : BaseStoxModel
    {
        public BlogPostYearModel()
        {
            Months = new List<BlogPostMonthModel>();
        }
        public int Year { get; set; }
        public IList<BlogPostMonthModel> Months { get; set; }
    }
    public partial class BlogPostMonthModel : BaseStoxModel
    {
        public int Month { get; set; }

        public int BlogPostCount { get; set; }
    }
}