﻿using System.Web.Routing;
using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Cms
{
    public partial class RenderWidgetModel : BaseStoxModel
    {
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
        public RouteValueDictionary RouteValues { get; set; }
    }
}