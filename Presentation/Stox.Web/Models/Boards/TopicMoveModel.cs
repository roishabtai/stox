﻿using System.Collections.Generic;
using System.Web.Mvc;
using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Boards
{
    public partial class TopicMoveModel : BaseStoxEntityModel
    {
        public TopicMoveModel()
        {
            ForumList = new List<SelectListItem>();
        }

        public int ForumSelected { get; set; }
        public string TopicSeName { get; set; }

        public IEnumerable<SelectListItem> ForumList { get; set; }
    }
}