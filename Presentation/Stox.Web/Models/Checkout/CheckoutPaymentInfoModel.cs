﻿using System.Web.Routing;
using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Checkout
{
    public partial class CheckoutPaymentInfoModel : BaseStoxModel
    {
        public string PaymentInfoActionName { get; set; }
        public string PaymentInfoControllerName { get; set; }
        public RouteValueDictionary PaymentInfoRouteValues { get; set; }

        /// <summary>
        /// Used on one-page checkout page
        /// </summary>
        public bool DisplayOrderTotals { get; set; }
    }
}