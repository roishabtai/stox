﻿using System.Collections.Generic;
using Stox.Web.Framework.Mvc;
using Stox.Web.Models.Common;

namespace Stox.Web.Models.Checkout
{
    public partial class CheckoutShippingAddressModel : BaseStoxModel
    {
        public CheckoutShippingAddressModel()
        {
            ExistingAddresses = new List<AddressModel>();
            NewAddress = new AddressModel();
        }

        public IList<AddressModel> ExistingAddresses { get; set; }

        public AddressModel NewAddress { get; set; }

        public bool NewAddressPreselected { get; set; }
    }
}