﻿using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Checkout
{
    public partial class OnePageCheckoutModel : BaseStoxModel
    {
        public bool ShippingRequired { get; set; }
    }
}