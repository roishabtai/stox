﻿using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Checkout
{
    public partial class CheckoutCompletedModel : BaseStoxModel
    {
        public int OrderId { get; set; }
        public bool OnePageCheckoutEnabled { get; set; }
    }
}