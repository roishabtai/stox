﻿using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Checkout
{
    public partial class CheckoutProgressModel : BaseStoxModel
    {
        public CheckoutProgressStep CheckoutProgressStep { get; set; }
    }

    public enum CheckoutProgressStep
    {
        Cart,
        Address,
        Shipping,
        Payment,
        Confirm,
        Complete
    }
}