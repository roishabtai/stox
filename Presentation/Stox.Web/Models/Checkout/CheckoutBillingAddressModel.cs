﻿using System.Collections.Generic;
using Stox.Web.Framework.Mvc;
using Stox.Web.Models.Common;

namespace Stox.Web.Models.Checkout
{
    public partial class CheckoutBillingAddressModel : BaseStoxModel
    {
        public CheckoutBillingAddressModel()
        {
            ExistingAddresses = new List<AddressModel>();
            NewAddress = new AddressModel();
        }

        public IList<AddressModel> ExistingAddresses { get; set; }

        public AddressModel NewAddress { get; set; }

        /// <summary>
        /// Used on one-page checkout page
        /// </summary>
        public bool NewAddressPreselected { get; set; }
    }
}