﻿using System.Collections.Generic;
using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Catalog
{
    public class CategorySimpleModel : BaseStoxEntityModel
    {
        public CategorySimpleModel()
        {
            SubCategories = new List<CategorySimpleModel>();
        }

        public string Name { get; set; }

        public string SeName { get; set; }

        public int? NumberOfProducts { get; set; }

        public List<CategorySimpleModel> SubCategories { get; set; }
    }
}