﻿using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Catalog
{
    public partial class AddToCompareListModel : BaseStoxModel
    {
        public int ProductId { get; set; }
    }
}