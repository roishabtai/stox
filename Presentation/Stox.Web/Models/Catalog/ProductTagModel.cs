﻿using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Catalog
{
    public partial class ProductTagModel : BaseStoxEntityModel
    {
        public string Name { get; set; }

        public string SeName { get; set; }

        public int ProductCount { get; set; }
    }
}