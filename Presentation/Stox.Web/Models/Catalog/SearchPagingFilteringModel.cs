﻿using Stox.Web.Framework.Mvc;
using Stox.Web.Framework.UI.Paging;

namespace Stox.Web.Models.Catalog
{
    public partial class SearchPagingFilteringModel : BasePageableModel
    {
        public class CategoryModel : BaseStoxEntityModel
        {
            public string Breadcrumb { get; set; }
        }
    }
}