﻿using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Catalog
{
    public partial class SearchBoxModel : BaseStoxModel
    {
        public bool AutoCompleteEnabled { get; set; }
        public bool ShowProductImagesInSearchAutoComplete { get; set; }
        public int SearchTermMinimumLength { get; set; }
    }
}