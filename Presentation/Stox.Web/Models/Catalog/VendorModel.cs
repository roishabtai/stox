﻿using System.Collections.Generic;
using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Catalog
{
    public partial class VendorModel : BaseStoxEntityModel
    {
        public VendorModel()
        {
            Products = new List<ProductOverviewModel>();
            PagingFilteringContext = new CatalogPagingFilteringModel();
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public string SeName { get; set; }


        public CatalogPagingFilteringModel PagingFilteringContext { get; set; }

        public IList<ProductOverviewModel> Products { get; set; }
    }
}