﻿using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Catalog
{
    public partial class ProductSpecificationModel : BaseStoxModel
    {
        public int SpecificationAttributeId { get; set; }

        public string SpecificationAttributeName { get; set; }

        public string SpecificationAttributeOption { get; set; }
    }
}