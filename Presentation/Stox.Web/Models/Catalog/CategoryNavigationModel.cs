﻿using System.Collections.Generic;
using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Catalog
{
    public partial class CategoryNavigationModel : BaseStoxModel
    {
        public CategoryNavigationModel()
        {
            Categories = new List<CategorySimpleModel>();
        }

        public int CurrentCategoryId { get; set; }
        public List<CategorySimpleModel> Categories { get; set; }
    }
}