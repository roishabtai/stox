﻿using System.Collections.Generic;
using Stox.Web.Models.Common;

namespace Stox.Web.Models.PrivateMessages
{
    public partial class PrivateMessageListModel
    {
        public IList<PrivateMessageModel> Messages { get; set; }
        public PagerModel PagerModel { get; set; }
    }
}