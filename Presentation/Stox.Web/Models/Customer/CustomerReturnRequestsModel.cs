﻿using System;
using System.Collections.Generic;
using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Customer
{
    public partial class CustomerReturnRequestsModel : BaseStoxModel
    {
        public CustomerReturnRequestsModel()
        {
            Items = new List<ReturnRequestModel>();
        }

        public IList<ReturnRequestModel> Items { get; set; }
        public CustomerNavigationModel NavigationModel { get; set; }

        #region Nested classes
        public partial class ReturnRequestModel : BaseStoxEntityModel
        {
            public string ReturnRequestStatus { get; set; }
            public int ProductId { get; set; }
            public string ProductName { get; set; }
            public string ProductSeName { get; set; }
            public int Quantity { get; set; }

            public string ReturnReason { get; set; }
            public string ReturnAction { get; set; }
            public string Comments { get; set; }

            public DateTime CreatedOn { get; set; }
        }
        #endregion
    }
}