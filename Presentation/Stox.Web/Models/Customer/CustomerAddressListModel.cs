﻿using System.Collections.Generic;
using Stox.Web.Framework.Mvc;
using Stox.Web.Models.Common;

namespace Stox.Web.Models.Customer
{
    public partial class CustomerAddressListModel : BaseStoxModel
    {
        public CustomerAddressListModel()
        {
            Addresses = new List<AddressModel>();
        }

        public IList<AddressModel> Addresses { get; set; }
        public CustomerNavigationModel NavigationModel { get; set; }
    }
}