﻿using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Customer
{
    public partial class CustomerAvatarModel : BaseStoxModel
    {
        public string AvatarUrl { get; set; }
        public CustomerNavigationModel NavigationModel { get; set; }
    }
}