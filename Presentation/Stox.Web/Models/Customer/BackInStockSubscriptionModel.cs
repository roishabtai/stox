﻿using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Customer
{
    public partial class BackInStockSubscriptionModel : BaseStoxEntityModel
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string SeName { get; set; }
    }
}
