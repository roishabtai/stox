﻿using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Customer
{
    public partial class RegisterResultModel : BaseStoxModel
    {
        public string Result { get; set; }
    }
}