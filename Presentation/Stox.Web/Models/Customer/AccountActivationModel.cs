﻿using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Customer
{
    public partial class AccountActivationModel : BaseStoxModel
    {
        public string Result { get; set; }
    }
}