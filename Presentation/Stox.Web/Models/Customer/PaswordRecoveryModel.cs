﻿using System.Web.Mvc;
using FluentValidation.Attributes;
using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;
using Stox.Web.Validators.Customer;

namespace Stox.Web.Models.Customer
{
    [Validator(typeof(PasswordRecoveryValidator))]
    public partial class PasswordRecoveryModel : BaseStoxModel
    {
        [AllowHtml]
        [StoxResourceDisplayName("Account.PasswordRecovery.Email")]
        public string Email { get; set; }

        public string Result { get; set; }
    }
}