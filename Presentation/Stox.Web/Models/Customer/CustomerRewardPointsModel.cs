﻿using System;
using System.Collections.Generic;
using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Customer
{
    public partial class CustomerRewardPointsModel : BaseStoxModel
    {
        public CustomerRewardPointsModel()
        {
            RewardPoints = new List<RewardPointsHistoryModel>();
        }

        public IList<RewardPointsHistoryModel> RewardPoints { get; set; }
        public int RewardPointsBalance { get; set; }
        public string RewardPointsAmount { get; set; }
        public int MinimumRewardPointsBalance { get; set; }
        public string MinimumRewardPointsAmount { get; set; }
        public CustomerNavigationModel NavigationModel { get; set; }

        #region Nested classes
        public partial class RewardPointsHistoryModel : BaseStoxEntityModel
        {
            [StoxResourceDisplayName("RewardPoints.Fields.Points")]
            public int Points { get; set; }

            [StoxResourceDisplayName("RewardPoints.Fields.PointsBalance")]
            public int PointsBalance { get; set; }

            [StoxResourceDisplayName("RewardPoints.Fields.Message")]
            public string Message { get; set; }

            [StoxResourceDisplayName("RewardPoints.Fields.Date")]
            public DateTime CreatedOn { get; set; }
        }

        #endregion
    }
}