﻿using Stox.Web.Framework.Mvc;
using Stox.Web.Models.Common;

namespace Stox.Web.Models.Customer
{
    public partial class CustomerAddressEditModel : BaseStoxModel
    {
        public CustomerAddressEditModel()
        {
            this.Address = new AddressModel();
        }
        public AddressModel Address { get; set; }
        public CustomerNavigationModel NavigationModel { get; set; }
    }
}