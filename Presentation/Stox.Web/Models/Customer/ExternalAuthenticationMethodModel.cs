﻿using System.Web.Routing;
using Stox.Web.Framework.Mvc;

namespace Stox.Web.Models.Customer
{
    public partial class ExternalAuthenticationMethodModel : BaseStoxModel
    {
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
        public RouteValueDictionary RouteValues { get; set; }
    }
}