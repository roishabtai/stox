﻿using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;

namespace Stox.Admin.Models.Orders
{
    public partial class NeverSoldReportLineModel : BaseStoxModel
    {
        public int ProductId { get; set; }
        [StoxResourceDisplayName("Admin.SalesReport.NeverSold.Fields.Name")]
        public string ProductName { get; set; }
    }
}