﻿using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Stox.Admin.Models.Orders
{
    public partial class GiftCardListModel : BaseStoxModel
    {
        public GiftCardListModel()
        {
            ActivatedList = new List<SelectListItem>();
        }

        [StoxResourceDisplayName("Admin.GiftCards.List.CouponCode")]
        [AllowHtml]
        public string CouponCode { get; set; }

        [StoxResourceDisplayName("Admin.GiftCards.List.Activated")]
        public int ActivatedId { get; set; }

        [StoxResourceDisplayName("Admin.GiftCards.List.Activated")]
        public IList<SelectListItem> ActivatedList { get; set; }
    }
}