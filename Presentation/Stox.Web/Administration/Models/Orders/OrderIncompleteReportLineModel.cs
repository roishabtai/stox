using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;

namespace Stox.Admin.Models.Orders
{
    public partial class OrderIncompleteReportLineModel : BaseStoxModel
    {
        [StoxResourceDisplayName("Admin.SalesReport.Incomplete.Item")]
        public string Item { get; set; }

        [StoxResourceDisplayName("Admin.SalesReport.Incomplete.Total")]
        public string Total { get; set; }

        [StoxResourceDisplayName("Admin.SalesReport.Incomplete.Count")]
        public int Count { get; set; }
    }
}
