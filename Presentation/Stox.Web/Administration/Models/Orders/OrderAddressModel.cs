﻿using Stox.Admin.Models.Common;
using Stox.Web.Framework.Mvc;

namespace Stox.Admin.Models.Orders
{
    public partial class OrderAddressModel : BaseStoxModel
    {
        public int OrderId { get; set; }

        public AddressModel Address { get; set; }
    }
}