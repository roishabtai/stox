﻿using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;
using System;
using System.Collections.Generic;

namespace Stox.Admin.Models.Orders
{
    public partial class ShipmentModel : BaseStoxEntityModel
    {
        public ShipmentModel()
        {
            this.Items = new List<ShipmentItemModel>();
        }
        [StoxResourceDisplayName("Admin.Orders.Shipments.ID")]
        public override int Id { get; set; }
        [StoxResourceDisplayName("Admin.Orders.Shipments.OrderID")]
        public int OrderId { get; set; }
        [StoxResourceDisplayName("Admin.Orders.Shipments.TotalWeight")]
        public string TotalWeight { get; set; }
        [StoxResourceDisplayName("Admin.Orders.Shipments.TrackingNumber")]
        public string TrackingNumber { get; set; }

        [StoxResourceDisplayName("Admin.Orders.Shipments.ShippedDate")]
        public string ShippedDate { get; set; }
        public bool CanShip { get; set; }
        public DateTime? ShippedDateUtc { get; set; }

        [StoxResourceDisplayName("Admin.Orders.Shipments.DeliveryDate")]
        public string DeliveryDate { get; set; }
        public bool CanDeliver { get; set; }
        public DateTime? DeliveryDateUtc { get; set; }

        public List<ShipmentItemModel> Items { get; set; }

        #region Nested classes

        public partial class ShipmentItemModel : BaseStoxEntityModel
        {
            public int OrderItemId { get; set; }
            public int ProductId { get; set; }
            [StoxResourceDisplayName("Admin.Orders.Shipments.Products.ProductName")]
            public string ProductName { get; set; }
            public string Sku { get; set; }
            public string AttributeInfo { get; set; }

            //weight of one item (product)
            [StoxResourceDisplayName("Admin.Orders.Shipments.Products.ItemWeight")]
            public string ItemWeight { get; set; }
            [StoxResourceDisplayName("Admin.Orders.Shipments.Products.ItemDimensions")]
            public string ItemDimensions { get; set; }

            public int QuantityToAdd { get; set; }
            public int QuantityOrdered { get; set; }
            [StoxResourceDisplayName("Admin.Orders.Shipments.Products.QtyShipped")]
            public int QuantityInThisShipment { get; set; }
            public int QuantityInAllShipments { get; set; }
        }
        #endregion
    }
}