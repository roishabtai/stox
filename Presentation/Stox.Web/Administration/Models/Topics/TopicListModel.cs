﻿using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Stox.Admin.Models.Topics
{
    public partial class TopicListModel : BaseStoxModel
    {
        public TopicListModel()
        {
            AvailableStores = new List<SelectListItem>();
        }

        [StoxResourceDisplayName("Admin.ContentManagement.Topics.List.SearchStore")]
        public int SearchStoreId { get; set; }
        public IList<SelectListItem> AvailableStores { get; set; }
    }
}