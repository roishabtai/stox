﻿using FluentValidation.Attributes;
using Stox.Admin.Models.Common;
using Stox.Admin.Validators.Affiliates;
using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;
using System;

namespace Stox.Admin.Models.Affiliates
{
    [Validator(typeof(AffiliateValidator))]
    public partial class AffiliateModel : BaseStoxEntityModel
    {
        public AffiliateModel()
        {
            Address = new AddressModel();
        }

        [StoxResourceDisplayName("Admin.Affiliates.Fields.ID")]
        public override int Id { get; set; }

        [StoxResourceDisplayName("Admin.Affiliates.Fields.URL")]
        public string Url { get; set; }
        
        [StoxResourceDisplayName("Admin.Affiliates.Fields.Active")]
        public bool Active { get; set; }

        public AddressModel Address { get; set; }

        #region Nested classes
        
        public partial class AffiliatedOrderModel : BaseStoxEntityModel
        {
            [StoxResourceDisplayName("Admin.Affiliates.Orders.Order")]
            public override int Id { get; set; }

            [StoxResourceDisplayName("Admin.Affiliates.Orders.OrderStatus")]
            public string OrderStatus { get; set; }

            [StoxResourceDisplayName("Admin.Affiliates.Orders.PaymentStatus")]
            public string PaymentStatus { get; set; }

            [StoxResourceDisplayName("Admin.Affiliates.Orders.ShippingStatus")]
            public string ShippingStatus { get; set; }

            [StoxResourceDisplayName("Admin.Affiliates.Orders.OrderTotal")]
            public string OrderTotal { get; set; }

            [StoxResourceDisplayName("Admin.Affiliates.Orders.CreatedOn")]
            public DateTime CreatedOn { get; set; }
        }

        public partial class AffiliatedCustomerModel : BaseStoxEntityModel
        {
            [StoxResourceDisplayName("Admin.Affiliates.Customers.Name")]
            public string Name { get; set; }
        }

        #endregion
    }
}