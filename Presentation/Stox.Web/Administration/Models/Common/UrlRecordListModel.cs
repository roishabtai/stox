﻿using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;
using System.Web.Mvc;

namespace Stox.Admin.Models.Common
{
    public partial class UrlRecordListModel : BaseStoxModel
    {
        [StoxResourceDisplayName("Admin.System.SeNames.Name")]
        [AllowHtml]
        public string SeName { get; set; }
    }
}