﻿using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;
using System.Web.Mvc;

namespace Stox.Admin.Models.Common
{
    public partial class UrlRecordModel : BaseStoxEntityModel
    {
        [StoxResourceDisplayName("Admin.System.SeNames.Name")]
        [AllowHtml]
        public string Name { get; set; }

        [StoxResourceDisplayName("Admin.System.SeNames.EntityId")]
        public int EntityId { get; set; }

        [StoxResourceDisplayName("Admin.System.SeNames.EntityName")]
        public string EntityName { get; set; }

        [StoxResourceDisplayName("Admin.System.SeNames.IsActive")]
        public bool IsActive { get; set; }

        [StoxResourceDisplayName("Admin.System.SeNames.Language")]
        public string Language { get; set; }
    }
}