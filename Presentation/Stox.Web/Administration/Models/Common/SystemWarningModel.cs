﻿using Stox.Web.Framework.Mvc;

namespace Stox.Admin.Models.Common
{
    public partial class SystemWarningModel : BaseStoxModel
    {
        public SystemWarningLevel Level { get; set; }

        public string Text { get; set; }
    }

    public enum SystemWarningLevel
    {
        Pass,
        Warning,
        Fail
    }
}