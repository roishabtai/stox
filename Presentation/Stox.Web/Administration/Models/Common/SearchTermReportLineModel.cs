using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;

namespace Stox.Admin.Models.Common
{
    public partial class SearchTermReportLineModel : BaseStoxModel
    {
        [StoxResourceDisplayName("Admin.SearchTermReport.Keyword")]
        public string Keyword { get; set; }

        [StoxResourceDisplayName("Admin.SearchTermReport.Count")]
        public int Count { get; set; }
    }
}
