﻿using Stox.Admin.Models.Localization;
using Stox.Web.Framework.Mvc;
using System.Collections.Generic;

namespace Stox.Admin.Models.Common
{
    public partial class LanguageSelectorModel : BaseStoxModel
    {
        public LanguageSelectorModel()
        {
            AvailableLanguages = new List<LanguageModel>();
        }

        public IList<LanguageModel> AvailableLanguages { get; set; }

        public LanguageModel CurrentLanguage { get; set; }
    }
}