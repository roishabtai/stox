﻿using Stox.Web.Framework.Mvc;
using System.Web.Routing;

namespace Stox.Admin.Models.Plugins
{
    public partial class MiscPluginModel : BaseStoxModel
    {
        public string FriendlyName { get; set; }

        public string ConfigurationActionName { get; set; }
        public string ConfigurationControllerName { get; set; }
        public RouteValueDictionary ConfigurationRouteValues { get; set; }
    }
}