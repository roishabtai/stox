﻿using FluentValidation.Attributes;
using Stox.Admin.Validators.Tax;
using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;
using System.Web.Mvc;

namespace Stox.Admin.Models.Tax
{
    [Validator(typeof(TaxCategoryValidator))]
    public partial class TaxCategoryModel : BaseStoxEntityModel
    {
        [StoxResourceDisplayName("Admin.Configuration.Tax.Categories.Fields.Name")]
        [AllowHtml]
        public string Name { get; set; }

        [StoxResourceDisplayName("Admin.Configuration.Tax.Categories.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }
    }
}