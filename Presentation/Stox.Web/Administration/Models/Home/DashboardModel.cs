﻿using Stox.Web.Framework.Mvc;

namespace Stox.Admin.Models.Home
{
    public partial class DashboardModel : BaseStoxModel
    {
        public bool IsLoggedInAsVendor { get; set; }
    }
}