﻿using Stox.Admin.Models.Stores;
using Stox.Web.Framework.Mvc;
using System.Collections.Generic;

namespace Stox.Admin.Models.Settings
{
    public partial class StoreScopeConfigurationModel : BaseStoxModel
    {
        public StoreScopeConfigurationModel()
        {
            Stores = new List<StoreModel>();
        }

        public int StoreId { get; set; }
        public IList<StoreModel> Stores { get; set; }
    }
}