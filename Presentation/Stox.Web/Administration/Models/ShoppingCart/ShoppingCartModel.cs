﻿using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;

namespace Stox.Admin.Models.ShoppingCart
{
    public partial class ShoppingCartModel : BaseStoxModel
    {
        [StoxResourceDisplayName("Admin.CurrentCarts.Customer")]
        public int CustomerId { get; set; }
        [StoxResourceDisplayName("Admin.CurrentCarts.Customer")]
        public string CustomerEmail { get; set; }

        [StoxResourceDisplayName("Admin.CurrentCarts.TotalItems")]
        public int TotalItems { get; set; }
    }
}