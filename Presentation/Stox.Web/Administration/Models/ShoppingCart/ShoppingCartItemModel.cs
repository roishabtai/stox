﻿using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;
using System;

namespace Stox.Admin.Models.ShoppingCart
{
    public partial class ShoppingCartItemModel : BaseStoxEntityModel
    {
        [StoxResourceDisplayName("Admin.CurrentCarts.Store")]
        public string Store { get; set; }
        [StoxResourceDisplayName("Admin.CurrentCarts.Product")]
        public int ProductId { get; set; }
        [StoxResourceDisplayName("Admin.CurrentCarts.Product")]
        public string ProductName { get; set; }

        [StoxResourceDisplayName("Admin.CurrentCarts.UnitPrice")]
        public string UnitPrice { get; set; }
        [StoxResourceDisplayName("Admin.CurrentCarts.Quantity")]
        public int Quantity { get; set; }
        [StoxResourceDisplayName("Admin.CurrentCarts.Total")]
        public string Total { get; set; }
        [StoxResourceDisplayName("Admin.CurrentCarts.UpdatedOn")]
        public DateTime UpdatedOn { get; set; }
    }
}