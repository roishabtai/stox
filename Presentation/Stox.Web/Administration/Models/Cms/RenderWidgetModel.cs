﻿using Stox.Web.Framework.Mvc;
using System.Web.Routing;

namespace Stox.Admin.Models.Cms
{
    public partial class RenderWidgetModel : BaseStoxModel
    {
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
        public RouteValueDictionary RouteValues { get; set; }
    }
}