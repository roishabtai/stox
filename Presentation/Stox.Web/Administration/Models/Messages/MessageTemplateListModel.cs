﻿using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Stox.Admin.Models.Messages
{
    public partial class MessageTemplateListModel : BaseStoxModel
    {
        public MessageTemplateListModel()
        {
            AvailableStores = new List<SelectListItem>();
        }

        [StoxResourceDisplayName("Admin.ContentManagement.MessageTemplates.List.SearchStore")]
        public int SearchStoreId { get; set; }
        public IList<SelectListItem> AvailableStores { get; set; }
    }
}