﻿using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;

namespace Stox.Admin.Models.Customers
{
    public partial class RegisteredCustomerReportLineModel : BaseStoxModel
    {
        [StoxResourceDisplayName("Admin.Customers.Reports.RegisteredCustomers.Fields.Period")]
        public string Period { get; set; }

        [StoxResourceDisplayName("Admin.Customers.Reports.RegisteredCustomers.Fields.Customers")]
        public int Customers { get; set; }
    }
}