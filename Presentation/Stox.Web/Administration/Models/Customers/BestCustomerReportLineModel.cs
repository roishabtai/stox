﻿using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;

namespace Stox.Admin.Models.Customers
{
    public partial class BestCustomerReportLineModel : BaseStoxModel
    {
        public int CustomerId { get; set; }
        [StoxResourceDisplayName("Admin.Customers.Reports.BestBy.Fields.Customer")]
        public string CustomerName { get; set; }

        [StoxResourceDisplayName("Admin.Customers.Reports.BestBy.Fields.OrderTotal")]
        public string OrderTotal { get; set; }

        [StoxResourceDisplayName("Admin.Customers.Reports.BestBy.Fields.OrderCount")]
        public decimal OrderCount { get; set; }
    }
}