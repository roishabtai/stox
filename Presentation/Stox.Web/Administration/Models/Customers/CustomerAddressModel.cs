﻿using Stox.Admin.Models.Common;
using Stox.Web.Framework.Mvc;

namespace Stox.Admin.Models.Customers
{
    public partial class CustomerAddressModel : BaseStoxModel
    {
        public int CustomerId { get; set; }

        public AddressModel Address { get; set; }
    }
}