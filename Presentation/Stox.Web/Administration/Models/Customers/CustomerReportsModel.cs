﻿using Stox.Web.Framework.Mvc;

namespace Stox.Admin.Models.Customers
{
    public partial class CustomerReportsModel : BaseStoxModel
    {
        public BestCustomersReportModel BestCustomersByOrderTotal { get; set; }
        public BestCustomersReportModel BestCustomersByNumberOfOrders { get; set; }
    }
}