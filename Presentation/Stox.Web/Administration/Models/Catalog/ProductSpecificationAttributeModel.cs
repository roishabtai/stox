﻿using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;
using System.Web.Mvc;

namespace Stox.Admin.Models.Catalog
{
    public partial class ProductSpecificationAttributeModel : BaseStoxEntityModel
    {
        [StoxResourceDisplayName("Admin.Catalog.Products.SpecificationAttributes.Fields.SpecificationAttribute")]
        [AllowHtml]
        public string SpecificationAttributeName { get; set; }

        [StoxResourceDisplayName("Admin.Catalog.Products.SpecificationAttributes.Fields.SpecificationAttributeOption")]
        [AllowHtml]
        public string SpecificationAttributeOptionName { get; set; }

        [StoxResourceDisplayName("Admin.Catalog.Products.SpecificationAttributes.Fields.CustomValue")]
        [AllowHtml]
        public string CustomValue { get; set; }

        [StoxResourceDisplayName("Admin.Catalog.Products.SpecificationAttributes.Fields.AllowFiltering")]
        public bool AllowFiltering { get; set; }

        [StoxResourceDisplayName("Admin.Catalog.Products.SpecificationAttributes.Fields.ShowOnProductPage")]
        public bool ShowOnProductPage { get; set; }

        [StoxResourceDisplayName("Admin.Catalog.Products.SpecificationAttributes.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }
    }
}