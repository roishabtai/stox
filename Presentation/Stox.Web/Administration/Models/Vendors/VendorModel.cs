﻿using FluentValidation.Attributes;
using Stox.Admin.Validators.Vendors;
using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;
using System.Collections.Generic;

namespace Stox.Admin.Models.Vendors
{
    [Validator(typeof(VendorValidator))]
    public partial class VendorModel : BaseStoxEntityModel
    {
        public VendorModel()
        {
            AssociatedCustomerEmails = new List<string>();
        }

        [StoxResourceDisplayName("Admin.Vendors.Fields.Name")]
        public string Name { get; set; }

        [StoxResourceDisplayName("Admin.Vendors.Fields.Email")]
        public string Email { get; set; }

        [StoxResourceDisplayName("Admin.Vendors.Fields.Description")]
        public string Description { get; set; }

        [StoxResourceDisplayName("Admin.Vendors.Fields.AdminComment")]
        public string AdminComment { get; set; }

        [StoxResourceDisplayName("Admin.Vendors.Fields.Active")]
        public bool Active { get; set; }

        [StoxResourceDisplayName("Admin.Vendors.Fields.AssociatedCustomerEmails")]
        public IList<string> AssociatedCustomerEmails { get; set; }
    }
}