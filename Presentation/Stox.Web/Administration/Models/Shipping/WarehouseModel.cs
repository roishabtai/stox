﻿using FluentValidation.Attributes;
using Stox.Admin.Models.Common;
using Stox.Admin.Validators.Shipping;
using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;
using System.Web.Mvc;

namespace Stox.Admin.Models.Shipping
{
    [Validator(typeof(WarehouseValidator))]
    public partial class WarehouseModel : BaseStoxEntityModel
    {
        public WarehouseModel()
        {
            this.Address = new AddressModel();
        }
        [StoxResourceDisplayName("Admin.Configuration.Shipping.Warehouses.Fields.Name")]
        [AllowHtml]
        public string Name { get; set; }

        [StoxResourceDisplayName("Admin.Configuration.Shipping.Warehouses.Fields.Address")]
        public AddressModel Address { get; set; }
    }
}