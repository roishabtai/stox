﻿using FluentValidation.Attributes;
using Stox.Admin.Validators.Templates;
using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;
using System.Web.Mvc;

namespace Stox.Admin.Models.Templates
{
    [Validator(typeof(ManufacturerTemplateValidator))]
    public partial class ManufacturerTemplateModel : BaseStoxEntityModel
    {
        [StoxResourceDisplayName("Admin.System.Templates.Manufacturer.Name")]
        [AllowHtml]
        public string Name { get; set; }

        [StoxResourceDisplayName("Admin.System.Templates.Manufacturer.ViewPath")]
        [AllowHtml]
        public string ViewPath { get; set; }

        [StoxResourceDisplayName("Admin.System.Templates.Manufacturer.DisplayOrder")]
        public int DisplayOrder { get; set; }
    }
}