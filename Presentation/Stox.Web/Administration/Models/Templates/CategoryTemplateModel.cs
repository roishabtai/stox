﻿using FluentValidation.Attributes;
using Stox.Admin.Validators.Templates;
using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;
using System.Web.Mvc;

namespace Stox.Admin.Models.Templates
{
    [Validator(typeof(CategoryTemplateValidator))]
    public partial class CategoryTemplateModel : BaseStoxEntityModel
    {
        [StoxResourceDisplayName("Admin.System.Templates.Category.Name")]
        [AllowHtml]
        public string Name { get; set; }

        [StoxResourceDisplayName("Admin.System.Templates.Category.ViewPath")]
        [AllowHtml]
        public string ViewPath { get; set; }

        [StoxResourceDisplayName("Admin.System.Templates.Category.DisplayOrder")]
        public int DisplayOrder { get; set; }
    }
}