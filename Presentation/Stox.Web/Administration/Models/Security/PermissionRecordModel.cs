﻿using Stox.Web.Framework.Mvc;

namespace Stox.Admin.Models.Security
{
    public partial class PermissionRecordModel : BaseStoxModel
    {
        public string Name { get; set; }
        public string SystemName { get; set; }
    }
}