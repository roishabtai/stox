﻿using FluentValidation;
using Stox.Admin.Models.Templates;
using Stox.Services.Localization;

namespace Stox.Admin.Validators.Templates
{
    public class CategoryTemplateValidator : AbstractValidator<CategoryTemplateModel>
    {
        public CategoryTemplateValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.System.Templates.Category.Name.Required"));
            RuleFor(x => x.ViewPath).NotEmpty().WithMessage(localizationService.GetResource("Admin.System.Templates.Category.ViewPath.Required"));
        }
    }
}