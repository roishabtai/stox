﻿using FluentValidation;
using Stox.Admin.Models.Orders;
using Stox.Services.Localization;

namespace Stox.Admin.Validators.Orders
{
    public class CheckoutAttributeValidator : AbstractValidator<CheckoutAttributeModel>
    {
        public CheckoutAttributeValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.Catalog.Attributes.CheckoutAttributes.Fields.Name.Required"));
        }
    }
}