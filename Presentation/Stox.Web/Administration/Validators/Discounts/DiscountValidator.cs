﻿using FluentValidation;
using Stox.Admin.Models.Discounts;
using Stox.Services.Localization;

namespace Stox.Admin.Validators.Discounts
{
    public class DiscountValidator : AbstractValidator<DiscountModel>
    {
        public DiscountValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.Promotions.Discounts.Fields.Name.Required"));
        }
    }
}