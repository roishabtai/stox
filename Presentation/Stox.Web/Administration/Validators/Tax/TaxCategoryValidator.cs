﻿using FluentValidation;
using Stox.Admin.Models.Tax;
using Stox.Services.Localization;

namespace Stox.Admin.Validators.Tax
{
    public class TaxCategoryValidator : AbstractValidator<TaxCategoryModel>
    {
        public TaxCategoryValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.Configuration.Tax.Categories.Fields.Name.Required"));
        }
    }
}