﻿using FluentValidation;
using Stox.Admin.Models.Catalog;
using Stox.Services.Localization;

namespace Stox.Admin.Validators.Catalog
{
    public class ProductAttributeValidator : AbstractValidator<ProductAttributeModel>
    {
        public ProductAttributeValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.Catalog.Attributes.ProductAttributes.Fields.Name.Required"));
        }
    }
}