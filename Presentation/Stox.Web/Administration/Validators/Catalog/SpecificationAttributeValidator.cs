﻿using FluentValidation;
using Stox.Admin.Models.Catalog;
using Stox.Services.Localization;

namespace Stox.Admin.Validators.Catalog
{
    public class SpecificationAttributeValidator : AbstractValidator<SpecificationAttributeModel>
    {
        public SpecificationAttributeValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.Catalog.Attributes.SpecificationAttributes.Fields.Name.Required"));
        }
    }
}