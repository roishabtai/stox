﻿using FluentValidation;
using Stox.Admin.Models.Catalog;
using Stox.Services.Localization;

namespace Stox.Admin.Validators.Catalog
{
    public class ProductTagValidator : AbstractValidator<ProductTagModel>
    {
        public ProductTagValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.Catalog.ProductTags.Fields.Name.Required"));
        }
    }
}