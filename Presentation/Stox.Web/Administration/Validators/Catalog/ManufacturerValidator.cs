﻿using FluentValidation;
using Stox.Admin.Models.Catalog;
using Stox.Services.Localization;

namespace Stox.Admin.Validators.Catalog
{
    public class ManufacturerValidator : AbstractValidator<ManufacturerModel>
    {
        public ManufacturerValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.Catalog.Manufacturers.Fields.Name.Required"));
        }
    }
}