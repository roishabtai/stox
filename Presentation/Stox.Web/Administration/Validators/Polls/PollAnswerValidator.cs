﻿using FluentValidation;
using Stox.Admin.Models.Polls;
using Stox.Services.Localization;

namespace Stox.Admin.Validators.Polls
{
    public class PollAnswerValidator : AbstractValidator<PollAnswerModel>
    {
        public PollAnswerValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.ContentManagement.Polls.Answers.Fields.Name.Required"));
        }
    }
}