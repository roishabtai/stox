﻿using FluentValidation;
using Stox.Admin.Models.Polls;
using Stox.Services.Localization;

namespace Stox.Admin.Validators.Polls
{
    public class PollValidator : AbstractValidator<PollModel>
    {
        public PollValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.ContentManagement.Polls.Fields.Name.Required"));
        }
    }
}