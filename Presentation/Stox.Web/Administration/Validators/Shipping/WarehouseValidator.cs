﻿using FluentValidation;
using Stox.Admin.Models.Shipping;
using Stox.Services.Localization;

namespace Stox.Admin.Validators.Shipping
{
    public class WarehouseValidator : AbstractValidator<WarehouseModel>
    {
        public WarehouseValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.Configuration.Shipping.Warehouses.Fields.Name.Required"));
        }
    }
}