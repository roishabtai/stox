﻿using FluentValidation;
using Stox.Admin.Models.Shipping;
using Stox.Services.Localization;

namespace Stox.Admin.Validators.Shipping
{
    public class ShippingMethodValidator : AbstractValidator<ShippingMethodModel>
    {
        public ShippingMethodValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.Configuration.Shipping.Methods.Fields.Name.Required"));
        }
    }
}