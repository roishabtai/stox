﻿using FluentValidation;
using Stox.Admin.Models.Shipping;
using Stox.Services.Localization;

namespace Stox.Admin.Validators.Shipping
{
    public class DeliveryDateValidator : AbstractValidator<DeliveryDateModel>
    {
        public DeliveryDateValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.Configuration.Shipping.DeliveryDates.Fields.Name.Required"));
        }
    }
}