﻿using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;

namespace Stox.Plugin.Shipping.ByWeight.Models
{
    public class ShippingByWeightListModel : BaseStoxModel
    {
        [StoxResourceDisplayName("Plugins.Shipping.ByWeight.Fields.LimitMethodsToCreated")]
        public bool LimitMethodsToCreated { get; set; }
        
    }
}