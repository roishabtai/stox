﻿using Stox.Web.Framework;

namespace Stox.Plugin.DiscountRules.HadSpentAmount.Models
{
    public class RequirementModel
    {
        [StoxResourceDisplayName("Plugins.DiscountRules.HadSpentAmount.Fields.Amount")]
        public decimal SpentAmount { get; set; }

        public int DiscountId { get; set; }

        public int RequirementId { get; set; }
    }
}