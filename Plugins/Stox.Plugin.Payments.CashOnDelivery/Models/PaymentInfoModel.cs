﻿using Stox.Web.Framework.Mvc;

namespace Stox.Plugin.Payments.CashOnDelivery.Models
{
    public class PaymentInfoModel : BaseStoxModel
    {
        public string DescriptionText { get; set; }
    }
}