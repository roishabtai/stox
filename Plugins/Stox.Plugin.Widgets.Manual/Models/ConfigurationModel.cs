﻿using System.Web.Mvc;
using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;

namespace Stox.Plugin.Payments.Manual.Models
{
    public class ConfigurationModel : BaseStoxModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }

        [StoxResourceDisplayName("Plugins.Payments.Manual.Fields.AdditionalFeePercentage")]
        public bool AdditionalFeePercentage { get; set; }
        public bool AdditionalFeePercentage_OverrideForStore { get; set; }

        [StoxResourceDisplayName("Plugins.Payments.Manual.Fields.AdditionalFee")]
        public decimal AdditionalFee { get; set; }
        public bool AdditionalFee_OverrideForStore { get; set; }

        public int TransactModeId { get; set; }
        [StoxResourceDisplayName("Plugins.Payments.Manual.Fields.TransactMode")]
        public SelectList TransactModeValues { get; set; }
        public bool TransactModeId_OverrideForStore { get; set; }
    }
}