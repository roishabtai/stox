﻿using Stox.Web.Framework;

namespace Stox.Plugin.Shipping.CanadaPost.Models
{
    public class CanadaPostShippingModel
    {
        [StoxResourceDisplayName("Plugins.Shipping.CanadaPost.Fields.Url")]
        public string Url { get; set; }

        [StoxResourceDisplayName("Plugins.Shipping.CanadaPost.Fields.Port")]
        public int Port { get; set; }

        [StoxResourceDisplayName("Plugins.Shipping.CanadaPost.Fields.CustomerId")]
        public string CustomerId { get; set; }
    }
}