using Autofac;
using Autofac.Core;
using Stox.Core.Caching;
using Stox.Core.Infrastructure;
using Stox.Core.Infrastructure.DependencyManagement;
using Stox.Plugin.Widgets.NivoSlider.Controllers;

namespace Stox.Plugin.Widgets.NivoSlider.Infrastructure
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder)
        {
            //we cache presentation models between requests
            builder.RegisterType<WidgetsNivoSliderController>()
                .WithParameter(ResolvedParameter.ForNamed<ICacheManager>("nop_cache_static"));
        }

        public int Order
        {
            get { return 2; }
        }
    }
}
