using Autofac;
using Autofac.Core;
using Stox.Core.Caching;
using Stox.Core.Infrastructure;
using Stox.Core.Infrastructure.DependencyManagement;

namespace Stox.Plugin.Tax.CountryStateZip.Infrastructure
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder)
        {
            //we cache presentation models between requests
            builder.RegisterType<CountryStateZipTaxProvider>()
                .WithParameter(ResolvedParameter.ForNamed<ICacheManager>("nop_cache_static"));
        }

        public int Order
        {
            get { return 2; }
        }
    }
}
