﻿//------------------------------------------------------------------------------
// Contributor(s): RJH 08/07/2009. 
//------------------------------------------------------------------------------

namespace Stox.Plugin.Shipping.USPS.Domain
{
    internal enum USPSPackageSize
    {
        Regular,
        Large,
    }
}
