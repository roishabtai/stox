﻿using Stox.Web.Framework.Mvc;

namespace Stox.Plugin.Payments.CheckMoneyOrder.Models
{
    public class PaymentInfoModel : BaseStoxModel
    {
        public string DescriptionText { get; set; }
    }
}