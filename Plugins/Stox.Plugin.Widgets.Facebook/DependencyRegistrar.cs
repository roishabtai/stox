using Autofac;
using Autofac.Integration.Mvc;
using Stox.Core.Infrastructure;
using Stox.Core.Infrastructure.DependencyManagement;
using Stox.Plugin.ExternalAuth.Facebook.Core;

namespace Stox.Plugin.ExternalAuth.Facebook
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder)
        {
            builder.RegisterType<FacebookProviderAuthorizer>().As<IOAuthProviderFacebookAuthorizer>().InstancePerHttpRequest();
        }

        public int Order
        {
            get { return 1; }
        }
    }
}
