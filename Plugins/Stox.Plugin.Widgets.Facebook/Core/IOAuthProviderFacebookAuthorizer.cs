using Stox.Services.Authentication.External;

namespace Stox.Plugin.ExternalAuth.Facebook.Core
{
    public interface IOAuthProviderFacebookAuthorizer : IExternalProviderAuthorizer
    {
    }
}