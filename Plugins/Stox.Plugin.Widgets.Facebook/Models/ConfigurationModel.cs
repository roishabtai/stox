﻿using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;

namespace Stox.Plugin.ExternalAuth.Facebook.Models
{
    public class ConfigurationModel : BaseStoxModel
    {
        [StoxResourceDisplayName("Plugins.ExternalAuth.Facebook.ClientKeyIdentifier")]
        public string ClientKeyIdentifier { get; set; }
        [StoxResourceDisplayName("Plugins.ExternalAuth.Facebook.ClientSecret")]
        public string ClientSecret { get; set; }
    }
}