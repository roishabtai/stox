﻿using Stox.Web.Framework;

namespace Stox.Plugin.DiscountRules.HasAllProducts.Models
{
    public class RequirementModel
    {
        [StoxResourceDisplayName("Plugins.DiscountRules.HasAllProducts.Fields.Products")]
        public string Products { get; set; }

        public int DiscountId { get; set; }

        public int RequirementId { get; set; }
    }
}