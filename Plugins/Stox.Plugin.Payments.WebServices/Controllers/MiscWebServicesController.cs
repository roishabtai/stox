﻿using System.Web.Mvc;
using Stox.Web.Framework.Controllers;

namespace Stox.Plugin.Misc.WebServices.Controllers
{
    [AdminAuthorize]
    public class MiscWebServicesController : Controller
    {
        public ActionResult Configure()
        {
            return View("Stox.Plugin.Misc.WebServices.Views.MiscWebServices.Configure");
        }
    }
}