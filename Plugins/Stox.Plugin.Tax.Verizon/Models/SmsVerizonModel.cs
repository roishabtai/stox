﻿using Stox.Web.Framework;

namespace Stox.Plugin.Sms.Verizon.Models
{
    public class SmsVerizonModel
    {
        [StoxResourceDisplayName("Plugins.Sms.Verizon.Fields.Enabled")]
        public bool Enabled { get; set; }
        
        [StoxResourceDisplayName("Plugins.Sms.Verizon.Fields.Email")]
        public string Email { get; set; }

        [StoxResourceDisplayName("Plugins.Sms.Verizon.Fields.TestMessage")]
        public string TestMessage { get; set; }
        public string TestSmsResult { get; set; }
    }
}