﻿using Stox.Web.Framework.Mvc;

namespace Stox.Plugin.Misc.FacebookShop.Models
{
    public partial class FooterModel : BaseStoxModel
    {
        public string StoreName { get; set; }
    }
}