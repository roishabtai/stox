﻿using System.Collections.Generic;
using System.Web.Mvc;
using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;

namespace Stox.Plugin.Misc.FacebookShop.Models
{
    //just a simplified copy of \Nop.Web\Models\Catalog\SearchModel.cs
    public partial class SearchModel : BaseStoxModel
    {
        public SearchModel()
        {
            PagingFilteringContext = new SearchPagingFilteringModel();
            Products = new List<ProductOverviewModel>();
        }

        public string Warning { get; set; }

        public bool NoResults { get; set; }

        /// <summary>
        /// Query string
        /// </summary>
        [StoxResourceDisplayName("Search.SearchTerm")]
        [AllowHtml]
        public string Q { get; set; }


        public SearchPagingFilteringModel PagingFilteringContext { get; set; }
        public IList<ProductOverviewModel> Products { get; set; }
    }
}