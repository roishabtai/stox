using Autofac;
using Autofac.Core;
using Stox.Core.Caching;
using Stox.Core.Infrastructure;
using Stox.Core.Infrastructure.DependencyManagement;
using Stox.Plugin.Misc.FacebookShop.Controllers;

namespace Stox.Plugin.Misc.FacebookShop.Infrastructure
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder)
        {
            //we cache presentation models between requests
            builder.RegisterType<MiscFacebookShopController>()
                .WithParameter(ResolvedParameter.ForNamed<ICacheManager>("nop_cache_static"));
        }

        public int Order
        {
            get { return 2; }
        }
    }
}
