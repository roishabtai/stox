﻿using System.Web.Mvc;
using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;

namespace Stox.Plugin.Payments.PurchaseOrder.Models
{
    public class PaymentInfoModel : BaseStoxModel
    {
        [StoxResourceDisplayName("Plugins.Payment.PurchaseOrder.PurchaseOrderNumber")]
        [AllowHtml]
        public string PurchaseOrderNumber { get; set; }
    }
}