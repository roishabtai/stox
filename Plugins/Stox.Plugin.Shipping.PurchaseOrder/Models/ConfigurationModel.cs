﻿using Stox.Web.Framework;
using Stox.Web.Framework.Mvc;

namespace Stox.Plugin.Payments.PurchaseOrder.Models
{
    public class ConfigurationModel : BaseStoxModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }

        [StoxResourceDisplayName("Plugins.Payment.PurchaseOrder.AdditionalFee")]
        public decimal AdditionalFee { get; set; }
        public bool AdditionalFee_OverrideForStore { get; set; }

        [StoxResourceDisplayName("Plugins.Payment.PurchaseOrder.AdditionalFeePercentage")]
        public bool AdditionalFeePercentage { get; set; }
        public bool AdditionalFeePercentage_OverrideForStore { get; set; }
    }
}