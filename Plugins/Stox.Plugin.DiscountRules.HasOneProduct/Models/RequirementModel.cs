﻿using Stox.Web.Framework;

namespace Stox.Plugin.DiscountRules.HasOneProduct.Models
{
    public class RequirementModel
    {
        [StoxResourceDisplayName("Plugins.DiscountRules.HasOneProduct.Fields.Products")]
        public string Products { get; set; }

        public int DiscountId { get; set; }

        public int RequirementId { get; set; }
    }
}