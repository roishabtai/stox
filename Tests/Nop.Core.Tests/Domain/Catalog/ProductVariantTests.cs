﻿using NUnit.Framework;
using Stox.Core.Domain.Catalog;
using Stox.Tests;

namespace Stox.Core.Tests.Domain.Catalog
{
    [TestFixture]
    public class ProductVariantTests
    {
        [Test]
        public void Can_parse_required_productvariant_ids()
        {
            var productVariant = new Product
            {
                RequiredProductIds = "1, 4,7 ,a,"
            };

            var ids = productVariant.ParseRequiredProductIds();
            ids.Length.ShouldEqual(3);
            ids[0].ShouldEqual(1);
            ids[1].ShouldEqual(4);
            ids[2].ShouldEqual(7);
        }
    }
}
