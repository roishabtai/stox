﻿using NUnit.Framework;
using Stox.Tests;
using System;
using System.Data.Entity;

namespace Stox.Data.Tests
{
    [TestFixture]
    public class SchemaTests
    {
        [Test]
        public void Can_generate_schema()
        {
            Database.SetInitializer<StoxObjectContext>(null);
            var ctx = new StoxObjectContext("Test");
            string result = ctx.CreateDatabaseScript();
            result.ShouldNotBeNull();
            Console.Write(result);
        }
    }
}
